/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Course;

import Business.Employee.Employee;
import Business.Role.CoERole;
import Business.Role.Role;
import Business.Role.TalentPoolRole;
import java.util.ArrayList;

/**
 *
 * @author Maxx
 */
public class JavaCourse extends Course {
    
    public JavaCourse(){
        super(Course.CourseType.Java.getValue());
    }
    
//    @Override
//    public ArrayList<Role> getSupportedRole() {
//        ArrayList<Role> roles = new ArrayList<>();
//        roles.add(new TalentPoolRole());
//        return roles;
//    }
}
