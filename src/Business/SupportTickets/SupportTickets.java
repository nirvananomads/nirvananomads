/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.SupportTickets;

/**
 *
 * @author neelambabel
 */
public abstract class SupportTickets {
    
    private String priorityValue;
    private int supportID;
    private String issue;
    private String Resolution;
    
    public SupportTickets(String priorityValue){
        this.priorityValue = priorityValue;
        
    }

    public String getPriorityValue() {
        return priorityValue;
    }

    public void setPriorityValue(String priorityValue) {
        this.priorityValue = priorityValue;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getResolution() {
        return Resolution;
    }

    public void setResolution(String Resolution) {
        this.Resolution = Resolution;
    }
    
    
    
    
    public enum Type{
        Low("Low Priority"), Medium("Medium Priority"), High("High Priority");
        
        private String value;
        
        private Type(String value){
            this.value = value;
        }

        
        public String getValue() {
            return value;
        }
        
        
        
        @Override
        public String toString(){
        return value;
        }
    }
    
    
    
}
