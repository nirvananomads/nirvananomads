/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkRequest;
import Business.SupportTickets.SupportTickets;
import Business.Role.Role;


/**
 *
 * @author neelambabel
 */
public class SupportWorkRequest extends WorkRequest {
    
    private String issue;
    private int supportId;
    private String resolution;
    private String priorityValue;
    private static int count = 1;
    
    public SupportWorkRequest(String priorityValue){
        this.priorityValue = priorityValue;
        
    }
    
    private Role role;
    
    
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }   

    public String getPriorityValue() {
        return priorityValue;
    }

    public void setPriorityValue(String priorityValue) {
        this.priorityValue = priorityValue;
    }
    
    public enum Type{
        Low("Low Priority"), Medium("Medium Priority"), High("High Priority");
        
        private String value;
        
        private Type(String value){
            this.value = value;
        }

        
        public String getValue() {
            return value;
        }
    }
    public SupportWorkRequest(){
      supportId = count;
      count++;
    }
    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
       this.resolution = resolution;
    }

    
    
    @Override
    public String toString(){
        return issue;
    }
   
}
