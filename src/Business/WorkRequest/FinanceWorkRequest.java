/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkRequest;

import Business.Role.Role;

/**
 *
 * @author neelambabel
 */
public class FinanceWorkRequest extends WorkRequest {
    
    private String remarks;
    private int numberOfExtraDaysWorked;
    private int reimbursementAmount; 
    private int bonusAmount;
    private int totalAmount;
    private String comments;
    
    
     private Role role;
    
    
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }   

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public int getNumberOfExtraDaysWorked() {
        return numberOfExtraDaysWorked;
    }

    public void setNumberOfExtraDaysWorked(int numberOfExtraDaysWorked) {
        this.numberOfExtraDaysWorked = numberOfExtraDaysWorked;
    }

    public int getReimbursementAmount() {
        return reimbursementAmount;
    }

    public void setReimbursementAmount(int reimbursementAmount) {
        this.reimbursementAmount = reimbursementAmount;
    }

    public int getBonusAmount() {
        return bonusAmount;
    }

    public void setBonusAmount(int bonusAmount) {
        this.bonusAmount = bonusAmount;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
    
    
    
      @Override
    public String toString(){
        return remarks;
    }
    
}
