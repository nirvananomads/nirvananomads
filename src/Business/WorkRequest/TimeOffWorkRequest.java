/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkRequest;

import Business.Role.Role;
import java.util.Date;

/**
 *
 * @author neelambabel
 */
public class TimeOffWorkRequest extends WorkRequest {
    
    private String remarks;
    private int numberOfDays;
    private Date dateFrom; //change to date format later
    private String reasonOfAbsence;
    private String decision;
    private String comments;
    
   public TimeOffWorkRequest(String reasonOfAbsence, String decision ){
       this.reasonOfAbsence = reasonOfAbsence;
       this.decision = decision;
        
    }
    
 
   
    private Role role;
    
    
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }   
    
     public enum LeaveType{
        SickLeave("Sick Leave"), Vacation("Vacation");
        
        private String value;
        
        private LeaveType(String value){
            this.value = value;
        }

        
        public String getValue() {
            return value;
        }
    }
     
     public enum DecisionType{
        Approve("Approved"), Reject("Rejected");
        
        private String value;
        
        private DecisionType(String value){
            this.value = value;
        }

        
        public String getValue() {
            return value;
        }
    }

     public TimeOffWorkRequest(){
      //supportId = count;
      //count++;
    }
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public int getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDecision() {
        return decision;
    }

    public void setDecision(String decision) {
        this.decision = decision;
    }

    public String getReasonOfAbsence() {
        return reasonOfAbsence;
    }

    public void setReasonOfAbsence(String reasonOfAbsence) {
        this.reasonOfAbsence = reasonOfAbsence;
    }
     
    
      @Override
    public String toString(){
        return remarks;
    }
   
}
