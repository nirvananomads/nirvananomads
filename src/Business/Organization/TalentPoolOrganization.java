/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.Role;
import Business.Role.TalentPoolRole;
import java.util.ArrayList;

/**
 *
 * @author Maxx
 */
public class TalentPoolOrganization extends Organization{
    
    public TalentPoolOrganization(){
        super(Organization.Type.TalentPool.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new TalentPoolRole());
        return roles;
    }
    
}
