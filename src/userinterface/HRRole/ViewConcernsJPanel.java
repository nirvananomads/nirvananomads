/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.HRRole;

import Business.Enterprise.Enterprise;
import Business.Organization.HROrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import Business.WorkRequest.ConcernWorkRequest;
import Business.WorkRequest.WorkRequest;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Maxx
 */
public class ViewConcernsJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ViewConcernsJPanel
     */
    private JPanel userProcessContainer;
    private UserAccount userAccount;
    private HROrganization organization;
    private Enterprise enterprise;
    
    public ViewConcernsJPanel(JPanel userProcessContainer, UserAccount userAccount, HROrganization organization, Enterprise enterprise) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.userAccount = userAccount;
        this.organization = organization;
        this.enterprise = enterprise;
        
        populateConcernsTable();
    }
    
    public void populateConcernsTable(){
        DefaultTableModel model = (DefaultTableModel) tblConcerns.getModel();
        model.setRowCount(0);
        for(Organization org : enterprise.getOrganizationDirectory().getOrganizationList()){
            for(UserAccount ua: org.getUserAccountDirectory().getUserAccountList()){
                for (WorkRequest request : ua.getWorkQueue().getWorkRequestList()){
                    if(request instanceof ConcernWorkRequest){
                        Object[] row = new Object[5];
                        row[0] = request;
                        row[1] = request.getSender().getEmployee();
                        row[2] = request.getSender().getRole();
                        row[3] = request.getReceiver() == null ? null : request.getReceiver().getEmployee();
                        row[4] = request.getStatus();
                        model.addRow(row);
                    }
                }
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblConcerns = new javax.swing.JTable();
        jLabel13 = new javax.swing.JLabel();
        btnRefresh = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        btnProcess = new javax.swing.JButton();
        btnAssign = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        tblConcerns.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        tblConcerns.setForeground(new java.awt.Color(45, 118, 232));
        tblConcerns.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Concerns", "Sender", "Sender's Role", "Receiver", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblConcerns);

        jLabel13.setBackground(new java.awt.Color(45, 118, 232));
        jLabel13.setFont(new java.awt.Font("Verdana", 0, 36)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8_Communicate_48px_2.png"))); // NOI18N
        jLabel13.setText("View Concerns");
        jLabel13.setOpaque(true);

        btnRefresh.setBackground(new java.awt.Color(255, 255, 255));
        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8_Refresh_48px_1.png"))); // NOI18N
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        btnBack.setBackground(new java.awt.Color(255, 255, 255));
        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8_Back_50px.png"))); // NOI18N
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        btnProcess.setBackground(new java.awt.Color(255, 255, 255));
        btnProcess.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        btnProcess.setForeground(new java.awt.Color(45, 118, 232));
        btnProcess.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8_Process_48px.png"))); // NOI18N
        btnProcess.setText("Process");
        btnProcess.setEnabled(false);
        btnProcess.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcessActionPerformed(evt);
            }
        });

        btnAssign.setBackground(new java.awt.Color(255, 255, 255));
        btnAssign.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        btnAssign.setForeground(new java.awt.Color(45, 118, 232));
        btnAssign.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8_Audit_50px.png"))); // NOI18N
        btnAssign.setText("Assign");
        btnAssign.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAssignActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 876, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnRefresh))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnAssign)
                        .addGap(18, 18, 18)
                        .addComponent(btnProcess)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnRefresh)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 58, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnBack, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnProcess, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnAssign)))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnAssignActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAssignActionPerformed
        int selectedRow = tblConcerns.getSelectedRow();
        
        if (selectedRow < 0){
            JOptionPane.showMessageDialog(null, "Please select a row first!", "Warning", JOptionPane.WARNING_MESSAGE);
        }else if(JOptionPane.showConfirmDialog(null, "Do you want to be assigned this concern request?", "Warning", JOptionPane.YES_NO_OPTION)== JOptionPane.YES_OPTION){
            for(Organization org : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(UserAccount ua: org.getUserAccountDirectory().getUserAccountList()){
                    for (WorkRequest request : ua.getWorkQueue().getWorkRequestList()){
                        if(request instanceof ConcernWorkRequest){
                            request = (WorkRequest) tblConcerns.getValueAt(selectedRow, 0);
                            if(request.getStatus().equals("Resolved")){
                                JOptionPane.showMessageDialog(null, "Concern has been resolved.", "Warning", JOptionPane.WARNING_MESSAGE);
                                return;
                            }else{
                                request.setReceiver(userAccount);
                                request.setStatus("Concern being taken care of...");
                                populateConcernsTable();
                                btnProcess.setEnabled(true);
                            }
                        }
                    }
                }
            }
        }
    }//GEN-LAST:event_btnAssignActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        populateConcernsTable();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnProcessActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcessActionPerformed
        int selectedRow = tblConcerns.getSelectedRow();
        
        if (selectedRow < 0){
            JOptionPane.showMessageDialog(null, "Please select a row first!", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        
        ConcernWorkRequest request = (ConcernWorkRequest)tblConcerns.getValueAt(selectedRow, 0);
        if(request.getStatus().equals("Resolved")){
            JOptionPane.showMessageDialog(null, "Concern has been resolved.", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }else{
            request.setStatus("Processing");
        }
        
        ProcessConcernsJPanel processConcernsJPanel = new ProcessConcernsJPanel(userProcessContainer, request);
        userProcessContainer.add("ProcessConcernsJPanel", processConcernsJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnProcessActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAssign;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnProcess;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblConcerns;
    // End of variables declaration//GEN-END:variables
}
