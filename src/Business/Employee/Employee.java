/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Employee;

import Business.Course.CourseDirectory;

/**
 *
 * @author Maxx
 */
public class Employee {
    
    private String firstName;
    private String lastName;
    private String dateOfBirth;
    private String address;
    private String email;
    private CourseDirectory courseDirectory;
    private static int count = 0;
    private int id;
    
    public Employee(){
        id = ++count;
//        count++;
    }

    public CourseDirectory getCourseDirectory() {
        return courseDirectory;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString(){
        return firstName+" "+lastName;
    }
}
