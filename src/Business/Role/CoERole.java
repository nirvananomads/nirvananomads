/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Course.Course;
import Business.Course.CourseDirectory;
import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.CoEOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;
import userinterface.CoERole.CoEWorkAreaJPanel;

/**
 *
 * @author Maxx
 */
public class CoERole extends Role{
    
    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
            return new CoEWorkAreaJPanel(userProcessContainer, account, (CoEOrganization)organization, enterprise);
    }
}
