/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkRequest;

/**
 *
 * @author Maxx
 */
public class ConcernWorkRequest extends WorkRequest{
    
    private String concernWork;

    public String getConcernWork() {
        return concernWork;
    }

    public void setConcernWork(String concernWork) {
        this.concernWork = concernWork;
    }
}
