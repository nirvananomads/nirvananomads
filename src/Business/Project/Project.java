/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Project;

/**
 *
 * @author neelambabel
 */
public class Project {
    
    private String name;
    private int projectCode;
    private static int count = 1;
    
    public Project(){
      projectCode = count;
      count++;
   }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProjectCode(int projectCode) {
        this.projectCode = projectCode;
    }

    
    public int getProjectCode() {
        return projectCode;
    }
    
     @Override
    public String toString(){
        return name;
    }


   
}
