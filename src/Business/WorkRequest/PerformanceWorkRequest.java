/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkRequest;

import Business.Course.Course;
import Business.Role.Role;


/**
 *
 * @author neelambabel
 */
public class PerformanceWorkRequest {
    
    private Course course;
    private String Performance;
    private String courseType;
    
    
     public PerformanceWorkRequest(String courseType ){
       this.courseType = courseType;
       
        
    }
     
       public enum Type{
        Java("Java"), Python("Python");
        
        private String value;
        
        private Type(String value){
            this.value = value;
        }

        
        public String getValue() {
            return value;
        }
    }

     
     private Role role;
    
    
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }   
     
    
     public PerformanceWorkRequest(){
      //supportId = count;
      //count++;
    }
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getPerformance() {
        return Performance;
    }

    public void setPerformance(String Performance) {
        this.Performance = Performance;
    }
    
    
    
}
