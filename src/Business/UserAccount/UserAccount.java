/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.UserAccount;

import Business.Course.Course;
import Business.Course.CourseDirectory;
import Business.Employee.Employee;
import Business.Organization.Organization;
import Business.Role.Role;
import Business.WorkRequest.WorkQueue;

/**
 *
 * @author Maxx
 */
public class UserAccount {
    
    private String username;
    private String password;
    private boolean graduated;
    private Employee employee;
    private Role role;
    private Course course;
    private Role.RoleType roleType;
    private WorkQueue workQueue; 
    private CourseDirectory courseDirectory;
    
    public UserAccount() {
        workQueue = new WorkQueue();
//        courseDirectory = new CourseDirectory();
        graduated = false;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Role.RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(Role.RoleType roleType) {
        this.roleType = roleType;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public boolean isGraduated() {
        return graduated;
    }

    public void setGraduated(boolean graduated) {
        this.graduated = graduated;
    }

    public Course getCourse() {
        return course;
    }

    public CourseDirectory getCourseDirectory() {
        return courseDirectory;
    }

    public void setCourseDirectory(CourseDirectory courseDirectory) {
        this.courseDirectory = courseDirectory;
    }

    @Override
    public String toString() {
        return username;
    }
}
