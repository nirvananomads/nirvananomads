/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkRequest;

import Business.Employee.Employee;
import Business.Organization.Organization;
import Business.Project.Project;
import Business.SupportTickets.SupportTickets;
import Business.UserAccount.UserAccount;
import java.util.Date;

/**
 *
 * @author Maxx
 */
public class WorkRequest {
    
    private String message;
    private UserAccount sender;
    private UserAccount receiver;
    private Employee employee;
    private String status;
    private Date requestDate;
    private Date resolveDate;
    private Project project;
    private Organization organization;
    private SupportTickets supportTicket;
    private String projectResult;
    private int projectCode;
    private String issue;
    private int supportId;
    private String priorityValue;
    private static int count = 1;

    public Employee getEmployee() {
        return employee;
    }
    
    public WorkRequest(String priorityValue){
        this.priorityValue = priorityValue;
        
    }
    
    public String getPriorityValue() {
        return priorityValue;
    }

    public void setPriorityValue(String priorityValue) {
        this.priorityValue = priorityValue;
    }
    
    public enum Type{
        Low("Low Priority"), Medium("Medium Priority"), High("High Priority");
        
        private String value;
        
        private Type(String value){
            this.value = value;
        }

        
        public String getValue() {
            return value;
        }
    }
    
    public int getProjectCode() {
        return projectCode;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    
    public WorkRequest(){
        requestDate = new Date();
        projectCode = count;
        supportId = count;
        count++;
    }
    
     public int getSupportId() {
        return supportId;
    }
     
    public String getProjectResult() {
        return projectResult;
    }

    public void setProjectResult(String projectResult) {
        this.projectResult = projectResult;
    }
    
    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
    
    public SupportTickets getSupportTicket() {
        return supportTicket;
    }

    public void setSupportTicket(SupportTickets supportTicket) {
        this.supportTicket = supportTicket;
    }
    
    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }
    
    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserAccount getSender() {
        return sender;
    }

    public void setSender(UserAccount sender) {
        this.sender = sender;
    }

    public UserAccount getReceiver() {
        return receiver;
    }

    public void setReceiver(UserAccount receiver) {
        this.receiver = receiver;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getResolveDate() {
        return resolveDate;
    }

    public void setResolveDate(Date resolveDate) {
        this.resolveDate = resolveDate;
    }
    
    @Override
    public String toString(){
        return message;
    }
}
