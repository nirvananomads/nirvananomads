/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Employee.Employee;
import Business.Role.SystemAdminRole;
import Business.UserAccount.UserAccount;

/**
 *
 * @author Maxx
 */
public class ConfigureASystem {
    
    public static EcoSystem configure(){
        EcoSystem system = EcoSystem.getInstance();
        
        Employee sysAdmin = system.getEmployeeDirectory().createEmployee("sysadmin", "sysadmin", "sysadmin", "sysadmin", "sysadmin");
//        Employee admin = system.getEmployeeDirectory().createEmployee("admin", "admin", "admin", "admin", "admin");
//        Employee coe = system.getEmployeeDirectory().createEmployee("coe", "coe", "coe", "coe", "coe");
//        Employee finance = system.getEmployeeDirectory().createEmployee("finance", "finance", "finance", "finance", "finance");
//        Employee hR = system.getEmployeeDirectory().createEmployee("hr", "hr", "hr", "hr", "hr");
//        Employee manager = system.getEmployeeDirectory().createEmployee("manager", "manager", "manager", "manager", "manager");
//        Employee support = system.getEmployeeDirectory().createEmployee("support", "support", "support", "support", "support");
//        Employee talentPool = system.getEmployeeDirectory().createEmployee("talentpool", "talentpool", "talentpool", "talentpool", "talentpool");
//        Employee tech = system.getEmployeeDirectory().createEmployee("tech", "tech", "tech", "tech", "tech");
        
        UserAccount accSysAdmin = system.getUserAccountDirectory().createUserAccount("sysadmin", "sysadmin", sysAdmin, new SystemAdminRole());
//        UserAccount accAdmin = system.getUserAccountDirectory().createUserAccount("admin", "admin", admin, new AdminRole());
//        UserAccount accCoE = system.getUserAccountDirectory().createUserAccount("coe", "coe", coe, new CoERole());
//        UserAccount accFinance = system.getUserAccountDirectory().createUserAccount("finance", "finance", finance, new FinanceRole());
//        UserAccount accHR = system.getUserAccountDirectory().createUserAccount("hr", "hr", hR, new HRRole());
//        UserAccount accManager = system.getUserAccountDirectory().createUserAccount("manager", "manager", manager, new ManagerRole());
//        UserAccount accSupport = system.getUserAccountDirectory().createUserAccount("support", "support", support, new SupportRole());
//        UserAccount accTalentPool = system.getUserAccountDirectory().createUserAccount("tp", "tp", talentPool, new TalentPoolRole());
//        UserAccount accTech = system.getUserAccountDirectory().createUserAccount("tech", "tech", tech, new TechRole());        
        
//        Organization coEOrganization = new CoEOrganization();
        
        
//        Course course1 = system.getCourseDirectory().createCourse(new Course("Course 1"));
//        Course course2 = system.getCourseDirectory().createCourse(new Course("Course 2"));
//        Course course3 = system.getCourseDirectory().createCourse(new Course("Course 3"));
        
//        coEOrganization.getCourseDirectory().createCourse(course1);
//        coEOrganization.getCourseDirectory().createCourse(course2);
//        coEOrganization.getCourseDirectory().createCourse(course3);
        
        return system;
    }
}
