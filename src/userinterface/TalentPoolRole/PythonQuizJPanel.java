/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.TalentPoolRole;

import Business.Course.Course;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.Organization.TalentPoolOrganization;
import Business.UserAccount.UserAccount;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Maxx
 */
public class PythonQuizJPanel extends javax.swing.JPanel {

    /**
     * Creates new form PythonQuizJPanel
     */
    private JPanel userProcessContainer;
    private UserAccount userAccount;
    private Organization organization;
    private Enterprise enterprise;
    
    public PythonQuizJPanel(JPanel userProcessContainer, UserAccount userAccount, Organization organization, Enterprise enterprise) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.userAccount = userAccount;
        this.organization = organization;
        this.enterprise = enterprise;
        radioButtonGroup();
    }
    
        private void radioButtonGroup(){
        ButtonGroup bg1 = new ButtonGroup();
        bg1.add(rBtn1Q1);
        bg1.add(rBtn1Q2);
        bg1.add(rBtn1Q3);
        bg1.add(rBtn1Q4);
        ButtonGroup bg2 = new ButtonGroup();
        bg2.add(rBtn2Q1);
        bg2.add(rBtn2Q2);
        bg2.add(rBtn2Q3);
        bg2.add(rBtn2Q4);
        ButtonGroup bg3 = new ButtonGroup();
        bg3.add(rBtn3Q1);
        bg3.add(rBtn3Q2);
        bg3.add(rBtn3Q3);
        bg3.add(rBtn3Q4);
        ButtonGroup bg4 = new ButtonGroup();
        bg4.add(rBtn4Q1);
        bg4.add(rBtn4Q2);
        bg4.add(rBtn4Q3);
        bg4.add(rBtn4Q4);
        ButtonGroup bg5 = new ButtonGroup();
        bg5.add(rBtn5Q1);
        bg5.add(rBtn5Q2);
        bg5.add(rBtn5Q3);
        bg5.add(rBtn5Q4);
    }

    public void result() {
        int result = 100;
        int deduct = 20;

        if (rBtn1Q3.isSelected() && rBtn2Q2.isSelected() && rBtn3Q2.isSelected() && rBtn4Q1.isSelected() && rBtn5Q3.isSelected()) {
            lblScore.setText(String.valueOf(result));
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(Course course : organization.getCourseDirectory().getCourseList()){
                    course.setPythonPerformance(result);
                }
            }

        }
        if (!rBtn1Q3.isSelected() && !rBtn2Q2.isSelected() && !rBtn3Q2.isSelected() && !rBtn4Q1.isSelected() && !rBtn5Q3.isSelected()) {
            lblScore.setText(String.valueOf(result - result));
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(Course course : organization.getCourseDirectory().getCourseList()){
                    course.setPythonPerformance(result - result);
                }
            }

        }
        if (rBtn1Q3.isSelected() && !rBtn2Q2.isSelected() && !rBtn3Q2.isSelected() && !rBtn4Q1.isSelected() && !rBtn5Q3.isSelected()) {
            lblScore.setText(String.valueOf(result - (deduct * 4)));
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(Course course : organization.getCourseDirectory().getCourseList()){
                    course.setPythonPerformance(result - (deduct * 4));
                }
            }
        }
        if (!rBtn1Q3.isSelected() && rBtn2Q2.isSelected() && !rBtn3Q2.isSelected() && !rBtn4Q1.isSelected() && !rBtn5Q3.isSelected()) {
            lblScore.setText(String.valueOf(result - (deduct * 4)));
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(Course course : organization.getCourseDirectory().getCourseList()){
                    course.setPythonPerformance(result - (deduct * 4));
                }
            }
        }
        if (!rBtn1Q3.isSelected() && !rBtn2Q2.isSelected() && rBtn3Q2.isSelected() && !rBtn4Q1.isSelected() && !rBtn5Q3.isSelected()) {
            lblScore.setText(String.valueOf(result - (deduct * 4)));
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(Course course : organization.getCourseDirectory().getCourseList()){
                    course.setPythonPerformance(result - (deduct * 4));
                }
            }
        }
        if (!rBtn1Q3.isSelected() && !rBtn2Q2.isSelected() && !rBtn3Q2.isSelected() && rBtn4Q1.isSelected() && !rBtn5Q3.isSelected()) {
            lblScore.setText(String.valueOf(result - (deduct * 4)));
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(Course course : organization.getCourseDirectory().getCourseList()){
                    course.setPythonPerformance(result - (deduct * 4));
                }
            }
        }
        if (!rBtn1Q3.isSelected() && !rBtn2Q2.isSelected() && !rBtn3Q2.isSelected() && !rBtn4Q1.isSelected() && rBtn5Q3.isSelected()) {
            lblScore.setText(String.valueOf(result - (deduct * 4)));
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(Course course : organization.getCourseDirectory().getCourseList()){
                    course.setPythonPerformance(result - (deduct * 4));
                }
            }

        }
        if (rBtn1Q3.isSelected() && rBtn2Q2.isSelected() && !rBtn3Q2.isSelected() && !rBtn4Q1.isSelected() && !rBtn5Q3.isSelected()) {
            lblScore.setText(String.valueOf(result - (deduct * 3)));
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(Course course : organization.getCourseDirectory().getCourseList()){
                    course.setPythonPerformance(result - (deduct * 3));
                }
            }
        }
        if (!rBtn1Q3.isSelected() && rBtn2Q2.isSelected() && rBtn3Q2.isSelected() && !rBtn4Q1.isSelected() && !rBtn5Q3.isSelected()) {
            lblScore.setText(String.valueOf(result - (deduct * 3)));
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(Course course : organization.getCourseDirectory().getCourseList()){
                    course.setPythonPerformance(result - (deduct * 3));
                }
            }
        }
        if (!rBtn1Q3.isSelected() && !rBtn2Q2.isSelected() && rBtn3Q2.isSelected() && rBtn4Q1.isSelected() && !rBtn5Q3.isSelected()) {
            lblScore.setText(String.valueOf(result - (deduct * 3)));
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(Course course : organization.getCourseDirectory().getCourseList()){
                    course.setPythonPerformance(result - (deduct * 3));
                }
            }
        }
        if (!rBtn1Q3.isSelected() && !rBtn2Q2.isSelected() && !rBtn3Q2.isSelected() && rBtn4Q1.isSelected() && rBtn5Q3.isSelected()) {
            lblScore.setText(String.valueOf(result - (deduct * 3)));
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(Course course : organization.getCourseDirectory().getCourseList()){
                    course.setPythonPerformance(result - (deduct * 3));
                }
            }
        }
        if (rBtn1Q3.isSelected() && !rBtn2Q2.isSelected() && !rBtn3Q2.isSelected() && !rBtn4Q1.isSelected() && rBtn5Q3.isSelected()) {
            lblScore.setText(String.valueOf(result - (deduct * 3)));
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(Course course : organization.getCourseDirectory().getCourseList()){
                    course.setPythonPerformance(result - (deduct * 3));
                }
            }

        }
        if (rBtn1Q3.isSelected() && rBtn2Q2.isSelected() && rBtn3Q2.isSelected() && !rBtn4Q1.isSelected() && !rBtn5Q3.isSelected()) {
            lblScore.setText(String.valueOf(result - (deduct * 2)));
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(Course course : organization.getCourseDirectory().getCourseList()){
                    course.setPythonPerformance(result - (deduct * 2));
                }
            }
        }
        if (!rBtn1Q3.isSelected() && rBtn2Q2.isSelected() && rBtn3Q2.isSelected() && rBtn4Q1.isSelected() && !rBtn5Q3.isSelected()) {
            lblScore.setText(String.valueOf(result - (deduct * 2)));
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(Course course : organization.getCourseDirectory().getCourseList()){
                    course.setPythonPerformance(result - (deduct * 2));
                }
            }
        }
        if (!rBtn1Q3.isSelected() && !rBtn2Q2.isSelected() && rBtn3Q2.isSelected() && rBtn4Q1.isSelected() && rBtn5Q3.isSelected()) {
            lblScore.setText(String.valueOf(result - (deduct * 2)));
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(Course course : organization.getCourseDirectory().getCourseList()){
                    course.setPythonPerformance(result - (deduct * 2));
                }
            }
        }
        if (rBtn1Q3.isSelected() && !rBtn2Q2.isSelected() && !rBtn3Q2.isSelected() && rBtn4Q1.isSelected() && rBtn5Q3.isSelected()) {
            lblScore.setText(String.valueOf(result - (deduct * 2)));
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(Course course : organization.getCourseDirectory().getCourseList()){
                    course.setPythonPerformance(result - (deduct * 2));
                }
            }
        }
        if (rBtn1Q3.isSelected() && rBtn2Q2.isSelected() && !rBtn3Q2.isSelected() && !rBtn4Q1.isSelected() && rBtn5Q3.isSelected()) {
            lblScore.setText(String.valueOf(result - (deduct * 2)));
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(Course course : organization.getCourseDirectory().getCourseList()){
                    course.setPythonPerformance(result - (deduct * 2));
                }
            }

        }
        if (rBtn1Q3.isSelected() && rBtn2Q2.isSelected() && rBtn3Q2.isSelected() && rBtn4Q1.isSelected() && !rBtn5Q3.isSelected()) {
            lblScore.setText(String.valueOf(result - deduct));
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(Course course : organization.getCourseDirectory().getCourseList()){
                    course.setPythonPerformance(result - deduct);
                }
            }
        }
        if (!rBtn1Q3.isSelected() && rBtn2Q2.isSelected() && rBtn3Q2.isSelected() && rBtn4Q1.isSelected() && rBtn5Q3.isSelected()) {
            lblScore.setText(String.valueOf(result - deduct));
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(Course course : organization.getCourseDirectory().getCourseList()){
                    course.setPythonPerformance(result - deduct);
                }
            }
        }
        if (rBtn1Q3.isSelected() && !rBtn2Q2.isSelected() && rBtn3Q2.isSelected() && rBtn4Q1.isSelected() && rBtn5Q3.isSelected()) {
            lblScore.setText(String.valueOf(result - deduct));
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(Course course : organization.getCourseDirectory().getCourseList()){
                    course.setPythonPerformance(result - deduct);
                }
            }
        }
        if (rBtn1Q3.isSelected() && rBtn2Q2.isSelected() && !rBtn3Q2.isSelected() && rBtn4Q1.isSelected() && rBtn5Q3.isSelected()) {
            lblScore.setText(String.valueOf(result - deduct));
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(Course course : organization.getCourseDirectory().getCourseList()){
                    course.setPythonPerformance(result - deduct);
                }
            }
        }
        if (rBtn1Q3.isSelected() && rBtn2Q2.isSelected() && rBtn3Q2.isSelected() && !rBtn4Q1.isSelected() && rBtn5Q3.isSelected()) {
            lblScore.setText(String.valueOf(result - deduct));
            for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                for(Course course : organization.getCourseDirectory().getCourseList()){
                    course.setPythonPerformance(result - deduct);
                }
            }
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        rBtn3Q3 = new javax.swing.JRadioButton();
        rBtn3Q4 = new javax.swing.JRadioButton();
        rBtn3Q1 = new javax.swing.JRadioButton();
        rBtn4Q3 = new javax.swing.JRadioButton();
        rBtn4Q4 = new javax.swing.JRadioButton();
        rBtn4Q1 = new javax.swing.JRadioButton();
        jLabel5 = new javax.swing.JLabel();
        rBtn4Q2 = new javax.swing.JRadioButton();
        rBtn5Q3 = new javax.swing.JRadioButton();
        rBtn5Q4 = new javax.swing.JRadioButton();
        jLabel2 = new javax.swing.JLabel();
        rBtn1Q1 = new javax.swing.JRadioButton();
        rBtn5Q1 = new javax.swing.JRadioButton();
        jLabel6 = new javax.swing.JLabel();
        rBtn1Q2 = new javax.swing.JRadioButton();
        rBtn5Q2 = new javax.swing.JRadioButton();
        rBtn1Q3 = new javax.swing.JRadioButton();
        rBtn1Q4 = new javax.swing.JRadioButton();
        jLabel3 = new javax.swing.JLabel();
        rBtn2Q2 = new javax.swing.JRadioButton();
        rBtn2Q3 = new javax.swing.JRadioButton();
        rBtn2Q4 = new javax.swing.JRadioButton();
        rBtn2Q1 = new javax.swing.JRadioButton();
        btnBack = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        rBtn3Q2 = new javax.swing.JRadioButton();
        jLabel13 = new javax.swing.JLabel();
        lblScore = new javax.swing.JLabel();
        btnViewResult = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        rBtn3Q3.setBackground(new java.awt.Color(255, 255, 255));
        rBtn3Q3.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        rBtn3Q3.setForeground(new java.awt.Color(45, 118, 232));
        rBtn3Q3.setText("random()");

        rBtn3Q4.setBackground(new java.awt.Color(255, 255, 255));
        rBtn3Q4.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        rBtn3Q4.setForeground(new java.awt.Color(45, 118, 232));
        rBtn3Q4.setText("randrange ([start,] stop [,step])");

        rBtn3Q1.setBackground(new java.awt.Color(255, 255, 255));
        rBtn3Q1.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        rBtn3Q1.setForeground(new java.awt.Color(45, 118, 232));
        rBtn3Q1.setText("choice(seq)");

        rBtn4Q3.setBackground(new java.awt.Color(255, 255, 255));
        rBtn4Q3.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        rBtn4Q3.setForeground(new java.awt.Color(45, 118, 232));
        rBtn4Q3.setText("32 and 32");

        rBtn4Q4.setBackground(new java.awt.Color(255, 255, 255));
        rBtn4Q4.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        rBtn4Q4.setForeground(new java.awt.Color(45, 118, 232));
        rBtn4Q4.setText("64 and 32");

        rBtn4Q1.setBackground(new java.awt.Color(255, 255, 255));
        rBtn4Q1.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        rBtn4Q1.setForeground(new java.awt.Color(45, 118, 232));
        rBtn4Q1.setText("isnumeric()");

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(45, 118, 232));
        jLabel5.setText("4. Which of the following function checks in a string that all characters are numeric?");

        rBtn4Q2.setBackground(new java.awt.Color(255, 255, 255));
        rBtn4Q2.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        rBtn4Q2.setForeground(new java.awt.Color(45, 118, 232));
        rBtn4Q2.setText("islower()");

        rBtn5Q3.setBackground(new java.awt.Color(255, 255, 255));
        rBtn5Q3.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        rBtn5Q3.setForeground(new java.awt.Color(45, 118, 232));
        rBtn5Q3.setText("3");

        rBtn5Q4.setBackground(new java.awt.Color(255, 255, 255));
        rBtn5Q4.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        rBtn5Q4.setForeground(new java.awt.Color(45, 118, 232));
        rBtn5Q4.setText("None.");

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(45, 118, 232));
        jLabel2.setText("1. Which of the following is correct about Python?");

        rBtn1Q1.setBackground(new java.awt.Color(255, 255, 255));
        rBtn1Q1.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        rBtn1Q1.setForeground(new java.awt.Color(45, 118, 232));
        rBtn1Q1.setText("It supports automatic garbage collection.");

        rBtn5Q1.setBackground(new java.awt.Color(255, 255, 255));
        rBtn5Q1.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        rBtn5Q1.setForeground(new java.awt.Color(45, 118, 232));
        rBtn5Q1.setText("1");

        jLabel6.setBackground(new java.awt.Color(255, 255, 255));
        jLabel6.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(45, 118, 232));
        jLabel6.setText("5. What is the output of L[2] if L = [1,2,3]?");

        rBtn1Q2.setBackground(new java.awt.Color(255, 255, 255));
        rBtn1Q2.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        rBtn1Q2.setForeground(new java.awt.Color(45, 118, 232));
        rBtn1Q2.setText("It can be easily integrated with C, C++, COM, ActiveX, CORBA, and Java.");

        rBtn5Q2.setBackground(new java.awt.Color(255, 255, 255));
        rBtn5Q2.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        rBtn5Q2.setForeground(new java.awt.Color(45, 118, 232));
        rBtn5Q2.setText("2");

        rBtn1Q3.setBackground(new java.awt.Color(255, 255, 255));
        rBtn1Q3.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        rBtn1Q3.setForeground(new java.awt.Color(45, 118, 232));
        rBtn1Q3.setText("Both of the above.");

        rBtn1Q4.setBackground(new java.awt.Color(255, 255, 255));
        rBtn1Q4.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        rBtn1Q4.setForeground(new java.awt.Color(45, 118, 232));
        rBtn1Q4.setText("None of the above.");

        jLabel3.setBackground(new java.awt.Color(255, 255, 255));
        jLabel3.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(45, 118, 232));
        jLabel3.setText("2. What is the output of print list if list = [ 'abcd', 786 , 2.23, 'john', 70.2 ]?");

        rBtn2Q2.setBackground(new java.awt.Color(255, 255, 255));
        rBtn2Q2.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        rBtn2Q2.setForeground(new java.awt.Color(45, 118, 232));
        rBtn2Q2.setText("[ 'abcd', 786 , 2.23, 'john', 70.2 ]");

        rBtn2Q3.setBackground(new java.awt.Color(255, 255, 255));
        rBtn2Q3.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        rBtn2Q3.setForeground(new java.awt.Color(45, 118, 232));
        rBtn2Q3.setText("Error");

        rBtn2Q4.setBackground(new java.awt.Color(255, 255, 255));
        rBtn2Q4.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        rBtn2Q4.setForeground(new java.awt.Color(45, 118, 232));
        rBtn2Q4.setText("None.");

        rBtn2Q1.setBackground(new java.awt.Color(255, 255, 255));
        rBtn2Q1.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        rBtn2Q1.setForeground(new java.awt.Color(45, 118, 232));
        rBtn2Q1.setText("list");

        btnBack.setBackground(new java.awt.Color(255, 255, 255));
        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8_Back_50px.png"))); // NOI18N
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jLabel4.setBackground(new java.awt.Color(255, 255, 255));
        jLabel4.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(45, 118, 232));
        jLabel4.setText("3. Which of the following function sets the integer starting value used in generating random numbers?");

        rBtn3Q2.setBackground(new java.awt.Color(255, 255, 255));
        rBtn3Q2.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        rBtn3Q2.setForeground(new java.awt.Color(45, 118, 232));
        rBtn3Q2.setText("seed([x])");

        jLabel13.setBackground(new java.awt.Color(45, 118, 232));
        jLabel13.setFont(new java.awt.Font("Verdana", 0, 36)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8_Python_48px_1.png"))); // NOI18N
        jLabel13.setText("Python Quiz");
        jLabel13.setOpaque(true);

        lblScore.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        lblScore.setForeground(new java.awt.Color(45, 118, 232));

        btnViewResult.setBackground(new java.awt.Color(255, 255, 255));
        btnViewResult.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        btnViewResult.setForeground(new java.awt.Color(45, 118, 232));
        btnViewResult.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8_Test_Passed_64px_1.png"))); // NOI18N
        btnViewResult.setText("View Result");
        btnViewResult.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewResultActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblScore, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(172, 172, 172)
                        .addComponent(btnViewResult))
                    .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(rBtn1Q3)
                                .addGap(18, 18, 18)
                                .addComponent(rBtn1Q4))
                            .addComponent(rBtn1Q2)
                            .addComponent(rBtn1Q1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel6)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(rBtn4Q2)
                                .addGap(18, 18, 18)
                                .addComponent(rBtn4Q1)
                                .addGap(18, 18, 18)
                                .addComponent(rBtn4Q3)
                                .addGap(18, 18, 18)
                                .addComponent(rBtn4Q4))
                            .addComponent(jLabel5)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(rBtn3Q1)
                                .addGap(18, 18, 18)
                                .addComponent(rBtn3Q4)
                                .addGap(18, 18, 18)
                                .addComponent(rBtn3Q3)
                                .addGap(18, 18, 18)
                                .addComponent(rBtn3Q2))
                            .addComponent(jLabel4)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(rBtn2Q2)
                                .addGap(18, 18, 18)
                                .addComponent(rBtn2Q1)
                                .addGap(18, 18, 18)
                                .addComponent(rBtn2Q3)
                                .addGap(18, 18, 18)
                                .addComponent(rBtn2Q4))
                            .addComponent(jLabel3)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(rBtn5Q1)
                                .addGap(18, 18, 18)
                                .addComponent(rBtn5Q2)
                                .addGap(18, 18, 18)
                                .addComponent(rBtn5Q3)
                                .addGap(18, 18, 18)
                                .addComponent(rBtn5Q4)))
                        .addGap(0, 151, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rBtn1Q1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rBtn1Q2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rBtn1Q3)
                    .addComponent(rBtn1Q4))
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rBtn2Q1)
                    .addComponent(rBtn2Q2)
                    .addComponent(rBtn2Q3)
                    .addComponent(rBtn2Q4))
                .addGap(18, 18, 18)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rBtn3Q1)
                    .addComponent(rBtn3Q4)
                    .addComponent(rBtn3Q3)
                    .addComponent(rBtn3Q2))
                .addGap(18, 18, 18)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rBtn4Q1)
                    .addComponent(rBtn4Q2)
                    .addComponent(rBtn4Q3)
                    .addComponent(rBtn4Q4))
                .addGap(18, 18, 18)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rBtn5Q1)
                    .addComponent(rBtn5Q2)
                    .addComponent(rBtn5Q3)
                    .addComponent(rBtn5Q4))
                .addGap(60, 60, 60)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnBack)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btnViewResult, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblScore, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        if (JOptionPane.showConfirmDialog(null, "Do you want to go back? Your answers will not be saved.", "Warning", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            userProcessContainer.remove(this);
            CardLayout layout = (CardLayout) userProcessContainer.getLayout();
            layout.previous(userProcessContainer);
        } else {
            return;
        }
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnViewResultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewResultActionPerformed
        ButtonGroup bg1 = new ButtonGroup();
        bg1.add(rBtn1Q1);
        bg1.add(rBtn1Q2);
        bg1.add(rBtn1Q3);
        bg1.add(rBtn1Q4);
        ButtonGroup bg2 = new ButtonGroup();
        bg2.add(rBtn2Q1);
        bg2.add(rBtn2Q2);
        bg2.add(rBtn2Q3);
        bg2.add(rBtn2Q4);
        ButtonGroup bg3 = new ButtonGroup();
        bg3.add(rBtn3Q1);
        bg3.add(rBtn3Q2);
        bg3.add(rBtn3Q3);
        bg3.add(rBtn3Q4);
        ButtonGroup bg4 = new ButtonGroup();
        bg4.add(rBtn4Q1);
        bg4.add(rBtn4Q2);
        bg4.add(rBtn4Q3);
        bg4.add(rBtn4Q4);
        ButtonGroup bg5 = new ButtonGroup();
        bg5.add(rBtn5Q1);
        bg5.add(rBtn5Q2);
        bg5.add(rBtn5Q3);
        bg5.add(rBtn5Q4);
        
        if(bg1.getSelection() == null || bg2.getSelection() == null || bg3.getSelection() == null || bg4.getSelection() == null || bg5.getSelection() == null){
            JOptionPane.showConfirmDialog(null, "You have not completed the quiz!.", "Warning", JOptionPane.WARNING_MESSAGE);
        }else if (JOptionPane.showConfirmDialog(null, "Do you want to go back and view your result?", "Information", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
            result();
            userProcessContainer.remove(this);
            Component[] componentArray = userProcessContainer.getComponents();
            Component component = componentArray[componentArray.length - 2];
            TalentPoolWorkAreaJPanel talentPoolWorkAreaJPanel = (TalentPoolWorkAreaJPanel) component;
            CardLayout layout = (CardLayout) userProcessContainer.getLayout();
            layout.previous(userProcessContainer);
        }else{
            return;
        }
    }//GEN-LAST:event_btnViewResultActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnViewResult;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel lblScore;
    private javax.swing.JRadioButton rBtn1Q1;
    private javax.swing.JRadioButton rBtn1Q2;
    private javax.swing.JRadioButton rBtn1Q3;
    private javax.swing.JRadioButton rBtn1Q4;
    private javax.swing.JRadioButton rBtn2Q1;
    private javax.swing.JRadioButton rBtn2Q2;
    private javax.swing.JRadioButton rBtn2Q3;
    private javax.swing.JRadioButton rBtn2Q4;
    private javax.swing.JRadioButton rBtn3Q1;
    private javax.swing.JRadioButton rBtn3Q2;
    private javax.swing.JRadioButton rBtn3Q3;
    private javax.swing.JRadioButton rBtn3Q4;
    private javax.swing.JRadioButton rBtn4Q1;
    private javax.swing.JRadioButton rBtn4Q2;
    private javax.swing.JRadioButton rBtn4Q3;
    private javax.swing.JRadioButton rBtn4Q4;
    private javax.swing.JRadioButton rBtn5Q1;
    private javax.swing.JRadioButton rBtn5Q2;
    private javax.swing.JRadioButton rBtn5Q3;
    private javax.swing.JRadioButton rBtn5Q4;
    // End of variables declaration//GEN-END:variables
}
