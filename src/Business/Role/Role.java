/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;

/**
 *
 * @author Maxx
 */
public abstract class Role {
    
//    private String name;
    
    public enum RoleType{
        Admin("Admin"), CoE("Center of Excellence"), Finance("Finance"), HR("Human Resources")
        , Manager("Manager"), Support("Support"), Tech("Tech"), TalentPool("Talent Pool");
        
        private String value;
        
        private RoleType(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }
        
        @Override
        public String toString(){
            return value;
        }
    }

//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//    
//    public Role(String name){
//        this.name = name;
//    }
    
    public abstract JPanel createWorkArea(JPanel userProcessContainer, UserAccount useraccount, 
            Organization organization, Enterprise enterprise, EcoSystem business);
    
    @Override
    public String toString(){
        return this.getClass().getSimpleName();
    }
}
