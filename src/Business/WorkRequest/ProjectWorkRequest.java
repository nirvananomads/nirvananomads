/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkRequest;

import Business.Role.Role;

/**
 *
 * @author Maxx
 */
public class ProjectWorkRequest extends WorkRequest{
    
    //private String projectResult;
    private Role role;
    
    /*
    public String getProjectResult() {
        return projectResult;
    }

    public void setProjectResult(String projectResult) {
        this.projectResult = projectResult;
    }
*/
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }   
}
