/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Course;

import Business.Employee.EmployeeDirectory;
import Business.UserAccount.UserAccount;
import Business.UserAccount.UserAccountDirectory;

/**
 *
 * @author Maxx
 */
public abstract class Course {
    
    private String courseName;
    private EmployeeDirectory employeeDirectory;
    private UserAccountDirectory userAccountDirectory;
    private int id;
    private static int count = 0;
    private int javaPerformance;
    private int pythonPerformance;
    
    public Course(String courseName){
        this.courseName = courseName;
        id = ++count;
//        count++;
    }
    
    public enum CourseType{
        Java("Java Course"), Python("Python Course");
        
        private String value;
        
        private CourseType(String value){
            this.value = value;
        }
        
        public String getValue() {
            return value;
        }
        
    }

    public int getJavaPerformance() {
        return javaPerformance;
    }

    public void setJavaPerformance(int javaPerformance) {
        this.javaPerformance = javaPerformance;
    }

    public int getPythonPerformance() {
        return pythonPerformance;
    }

    public void setPythonPerformance(int pythonPerformance) {
        this.pythonPerformance = pythonPerformance;
    }

    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public void setUserAccountDirectory(UserAccountDirectory userAccountDirectory) {
        this.userAccountDirectory = userAccountDirectory;
    }
    
    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }
    
    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getId() {
        return id;
    }
    
    @Override
    public String toString(){
        return courseName;
    }
}
