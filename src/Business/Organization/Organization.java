/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Course.CourseDirectory;
import Business.Employee.EmployeeDirectory;
import Business.Project.ProjectDirectory;
import Business.Role.Role;
import Business.UserAccount.UserAccountDirectory;
import Business.WorkRequest.WorkQueue;
import java.util.ArrayList;

/**
 *
 * @author Maxx
 */
public abstract class Organization {
    
    private String name;
    private WorkQueue workQueue;
    private EmployeeDirectory employeeDirectory;
    private ProjectDirectory projectDirectory;
    private UserAccountDirectory userAccountDirectory;
    private CourseDirectory courseDirectory;
    private static int counter = 0;
    private int organizationID;
    
    public Organization(String name){
        this.name = name;
        workQueue = new WorkQueue();
        employeeDirectory = new EmployeeDirectory();
        projectDirectory = new ProjectDirectory();
        userAccountDirectory = new UserAccountDirectory();
        courseDirectory = new CourseDirectory();
        organizationID = ++counter;
//        counter++;
    }
    
    public enum Type{
        Admin("Admin Organization"), CoE("Center of Excellence Organization"), Finance("Finance Organization")
        , HR("HR Organization"), Manager("Manager Organization"), Support("Support Organization"), Tech("Tech Organization")
        , TalentPool("Talent Pool Organization");
        
        private String value;
        
        private Type(String value){
            this.value = value;
        }
        
        public String getValue() {
            return value;
        }
    }
    
    public CourseDirectory getCourseDirectory() {
        return courseDirectory;
    }
    
    public abstract ArrayList<Role> getSupportedRole();
    
    public UserAccountDirectory getUserAccountDirectory(){
        return userAccountDirectory;
    }
    
    public int getOrganizationID(){
        return organizationID;
    }
    
    public EmployeeDirectory getEmployeeDirectory(){
        return employeeDirectory;
    }

     public ProjectDirectory getProjectDirectory() {
        return projectDirectory;
    }

    public void setProjectDirectory(ProjectDirectory projectDirectory) {
        this.projectDirectory = projectDirectory;
    }
    
    public String getName() {
        return name;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }
    
    public void setType(Type orgType){
        name = String.valueOf(orgType);
    }
    
    @Override
    public String toString(){
        return name;
    }
}
