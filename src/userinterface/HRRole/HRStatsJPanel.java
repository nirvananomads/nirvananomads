/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.HRRole;

import Business.Enterprise.Enterprise;
import Business.Organization.HROrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import Business.WorkRequest.ConcernWorkRequest;
import Business.WorkRequest.WorkRequest;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.util.Rotation;

/**
 *
 * @author Maxx
 */
public class HRStatsJPanel extends javax.swing.JPanel {

    /**
     * Creates new form HRStatsJPanel
     */
    private JPanel userProcessContainer;
    private UserAccount userAccount;
    private HROrganization organization;
    private Enterprise enterprise;
    
    public HRStatsJPanel(JPanel userProcessContainer, UserAccount userAccount, HROrganization organization, Enterprise enterprise) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.userAccount = userAccount;
        this.organization = organization;
        this.enterprise = enterprise;
        populateConcernsTable();
    }
    
    public void populateConcernsTable(){
        DefaultTableModel model = (DefaultTableModel) tblConcerns.getModel();
        model.setRowCount(0);
        for(Organization org : enterprise.getOrganizationDirectory().getOrganizationList()){
            for(UserAccount ua: org.getUserAccountDirectory().getUserAccountList()){
                for (WorkRequest request : ua.getWorkQueue().getWorkRequestList()){
                    if(request instanceof ConcernWorkRequest){
                        Object[] row = new Object[5];
                        row[0] = request;
                        row[1] = request.getSender().getEmployee();
                        row[2] = request.getSender().getRole();
                        row[3] = request.getReceiver() == null ? null : request.getReceiver().getEmployee();
                        row[4] = request.getStatus();
                        model.addRow(row);
                    }
                }
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel12 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblConcerns = new javax.swing.JTable();
        backBtn = new javax.swing.JButton();
        btnStats = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabel12.setBackground(new java.awt.Color(45, 118, 232));
        jLabel12.setFont(new java.awt.Font("Verdana", 0, 36)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8_Statistics_48px_1.png"))); // NOI18N
        jLabel12.setText("Numbers in Stats");
        jLabel12.setOpaque(true);

        jLabel1.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(45, 118, 232));
        jLabel1.setText("Concerns by Role");

        tblConcerns.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        tblConcerns.setForeground(new java.awt.Color(45, 118, 232));
        tblConcerns.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Concerns", "Sender", "Sender's Role", "Receiver", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblConcerns);

        backBtn.setBackground(new java.awt.Color(255, 255, 255));
        backBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8_Back_50px.png"))); // NOI18N
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });

        btnStats.setBackground(new java.awt.Color(255, 255, 255));
        btnStats.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8_Statistics_48px.png"))); // NOI18N
        btnStats.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStatsActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, 876, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 876, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(backBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnStats)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 97, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(backBtn, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnStats, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backBtnActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backBtnActionPerformed

    private void btnStatsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStatsActionPerformed
        DefaultPieDataset dataset = new DefaultPieDataset();
        for(Organization org : enterprise.getOrganizationDirectory().getOrganizationList()){
            for(UserAccount ua: org.getUserAccountDirectory().getUserAccountList()){
                for (WorkRequest request : ua.getWorkQueue().getWorkRequestList()){
                    if(request instanceof ConcernWorkRequest){
                        String role = String.valueOf(request.getSender().getRole());
                        dataset.setValue(role, ua.getWorkQueue().getWorkRequestList().size());
                    }
                }
            }
        }
//        for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
//            String org = String.valueOf(organization.getName());
//            dataset.setValue(org, organization.getEmployeeDirectory().getEmployeeList().size());
//        }
        JFreeChart pieChart = ChartFactory.createPieChart3D("Concerns by Role", dataset, true, true, false);
        PiePlot3D plot = (PiePlot3D) pieChart.getPlot();
        plot.setStartAngle(90);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setForegroundAlpha(0.8f);
        ChartFrame frame = new ChartFrame("Concerns by Role", pieChart);
        //        pieChart.setBackgroundPaint(Color.WHITE);
        //        pieChart.getLegend().setItemPaint(Color.WHITE);
        frame.setVisible(true);
        frame.setSize(900, 600);
        frame.setBackground(Color.WHITE);
        Font f = new Font("Verdana", Font.BOLD, 14);
        frame.setFont(f);
    }//GEN-LAST:event_btnStatsActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backBtn;
    private javax.swing.JButton btnStats;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblConcerns;
    // End of variables declaration//GEN-END:variables
}
