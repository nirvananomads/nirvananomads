/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.CoERole;

import Business.Employee.Employee;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.Role.Role;
import Business.UserAccount.UserAccount;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Maxx
 */
public class TalentPoolGradJPanel extends javax.swing.JPanel {

    /**
     * Creates new form TalentPoolGradJPanel
     */
    private JPanel userProcessContainer;
    private UserAccount ua;
    private UserAccount talentUA;
    private Organization org;
    private Enterprise enterprise;
    
    public TalentPoolGradJPanel(JPanel userProcessContainer, UserAccount ua, UserAccount talentUA, Organization org, Enterprise enterprise) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.ua = ua;
        this.talentUA = talentUA;
        this.org = org;
        this.enterprise = enterprise;
        init();
//        for(Organization org : enterprise.getOrganizationDirectory().getOrganizationList()){
//            for(UserAccount ua : org.getUserAccountDirectory().getUserAccountList()){
//                if(org instanceof TalentPoolOrganization){
//                    
//                }
//            }
//        }
    }
    
    private void init(){
        popComboOrg();
//        chkCOEOrg.setSelected(org instanceof CoEOrganization);
//        chkFinanceOrg.setSelected(org instanceof FinanceOrganization);
//        chkHROrg.setSelected(org instanceof HROrganization);
//        chkManagerOrg.setSelected(org instanceof ManagerOrganization);
//        chkSupportOrg.setSelected(org instanceof SupportOrganization);
//        chkTechOrg.setSelected(org instanceof TechOrganization);
        
        txtFirstName.setText(talentUA.getEmployee().getFirstName());
        txtLastName.setText(talentUA.getEmployee().getLastName());
        txtDOB.setText(talentUA.getEmployee().getDateOfBirth());
        txtAdd.setText(talentUA.getEmployee().getAddress());
        txtEmail.setText(talentUA.getEmployee().getEmail());
        
//        chkCOERole.setSelected(talentUA.getRole() instanceof CoERole);
//        chkFinanceRole.setSelected(talentUA.getRole() instanceof FinanceRole);
//        chkHRRole.setSelected(talentUA.getRole() instanceof HRRole);
//        chkManagerRole.setSelected(talentUA.getRole() instanceof ManagerRole);
//        chkSupportRole.setSelected(talentUA.getRole() instanceof SupportRole);
//        chkTechRole.setSelected(talentUA.getRole() instanceof TechRole);
        
//        txtRole.setText(talentUA.getRole().toString());
//        txtRole.setText(String.valueOf(talentUA.getRole()));
//        txtRole.setText(org.getSupportedRole().toString()); 

        txtUsername.setText(talentUA.getUsername());
        txtPassword.setText(talentUA.getPassword()); 
        
    }
    
    public void popComboOrg(){
        cmbOrg.removeAllItems();
        for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
            if(!organization.getName().equals("Talent Pool Organization"))
            cmbOrg.addItem(organization);
        }
    }
    
    private void popComboRole(Organization organization){
        cmbRole.removeAllItems();
        cmbRole.addItem("");
        for(Role role : organization.getSupportedRole()){
            cmbRole.addItem(role);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel13 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        btnGrad = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtFirstName = new javax.swing.JTextField();
        txtLastName = new javax.swing.JTextField();
        txtDOB = new javax.swing.JTextField();
        txtAdd = new javax.swing.JTextField();
        txtEmail = new javax.swing.JTextField();
        txtUsername = new javax.swing.JTextField();
        txtPassword = new javax.swing.JTextField();
        btnUpdate = new javax.swing.JButton();
        cmbRole = new javax.swing.JComboBox();
        cmbOrg = new javax.swing.JComboBox();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabel13.setBackground(new java.awt.Color(45, 118, 232));
        jLabel13.setFont(new java.awt.Font("Verdana", 0, 36)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8_Graduation_Cap_48px_1.png"))); // NOI18N
        jLabel13.setText("Talent Pool Graduation");
        jLabel13.setOpaque(true);

        jLabel1.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(45, 118, 232));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Organization ");

        btnGrad.setBackground(new java.awt.Color(255, 255, 255));
        btnGrad.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        btnGrad.setForeground(new java.awt.Color(45, 118, 232));
        btnGrad.setText("Graduate");
        btnGrad.setEnabled(false);
        btnGrad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGradActionPerformed(evt);
            }
        });

        btnBack.setBackground(new java.awt.Color(255, 255, 255));
        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8_Back_50px.png"))); // NOI18N
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(45, 118, 232));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("First Name");

        jLabel3.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(45, 118, 232));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Last Name");

        jLabel4.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(45, 118, 232));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Date of Birth");

        jLabel5.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(45, 118, 232));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Address");

        jLabel6.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(45, 118, 232));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Email");

        jLabel7.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(45, 118, 232));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Role");

        jLabel8.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(45, 118, 232));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Username");

        jLabel9.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(45, 118, 232));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Password");

        txtFirstName.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        txtFirstName.setForeground(new java.awt.Color(45, 118, 232));
        txtFirstName.setEnabled(false);

        txtLastName.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        txtLastName.setForeground(new java.awt.Color(45, 118, 232));
        txtLastName.setEnabled(false);

        txtDOB.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        txtDOB.setForeground(new java.awt.Color(45, 118, 232));
        txtDOB.setEnabled(false);

        txtAdd.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        txtAdd.setForeground(new java.awt.Color(45, 118, 232));
        txtAdd.setEnabled(false);

        txtEmail.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        txtEmail.setForeground(new java.awt.Color(45, 118, 232));
        txtEmail.setEnabled(false);

        txtUsername.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        txtUsername.setForeground(new java.awt.Color(45, 118, 232));
        txtUsername.setEnabled(false);

        txtPassword.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        txtPassword.setForeground(new java.awt.Color(45, 118, 232));
        txtPassword.setEnabled(false);

        btnUpdate.setBackground(new java.awt.Color(255, 255, 255));
        btnUpdate.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        btnUpdate.setForeground(new java.awt.Color(45, 118, 232));
        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        cmbRole.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        cmbRole.setForeground(new java.awt.Color(45, 118, 232));
        cmbRole.setEnabled(false);

        cmbOrg.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        cmbOrg.setForeground(new java.awt.Color(45, 118, 232));
        cmbOrg.setEnabled(false);
        cmbOrg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbOrgActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnUpdate)
                        .addGap(18, 18, 18)
                        .addComponent(btnGrad))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(84, 84, 84)
                        .addComponent(txtPassword))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(77, 77, 77)
                        .addComponent(txtLastName))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(60, 60, 60)
                        .addComponent(txtDOB))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(93, 93, 93)
                        .addComponent(txtAdd))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(115, 115, 115)
                        .addComponent(txtEmail))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel7))
                        .addGap(81, 81, 81)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtUsername)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cmbRole, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 365, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1))
                        .addGap(57, 57, 57)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtFirstName)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cmbOrg, javax.swing.GroupLayout.PREFERRED_SIZE, 359, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cmbOrg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtFirstName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtLastName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtDOB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtAdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(cmbRole, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 66, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnBack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnUpdate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnGrad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnGradActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGradActionPerformed
        if(txtFirstName.getText().equals("") || txtLastName.getText().equals("") || 
                txtDOB.getText().equals("") || txtAdd.getText().equals("") || 
                txtEmail.getText().equals("") || txtUsername.getText().equals("") || 
                txtPassword.getText().equals("") || cmbRole.getSelectedItem().equals("")){
            JOptionPane.showMessageDialog(null, "There is one or more empty fields!", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        
//        if(chkCOEOrg.isSelected()){
//            org.setType(Organization.Type.CoE);
//        }
//        if(chkFinanceOrg.isSelected()){
//            org.setType(Organization.Type.Finance);
//        }
//        if(chkHROrg.isSelected()){
//            org.setType(Organization.Type.HR);
//        }
//        if(chkManagerOrg.isSelected()){
//            org.setType(Organization.Type.Manager);
//        }
//        if(chkSupportOrg.isSelected()){
//            org.setType(Organization.Type.Support);
//        }
//        if(chkTechOrg.isSelected()){
//            org.setType(Organization.Type.Tech);
//        }   
        
        String username = txtUsername.getText();
        String password = txtPassword.getText();
        org = (Organization) cmbOrg.getSelectedItem();
        Employee employee = talentUA.getEmployee();
        Role role = (Role) cmbRole.getSelectedItem();
        
        if(org.getUserAccountDirectory().checkIfUsernameIsUnique(username) == false){
            JOptionPane.showMessageDialog(null, "Please enter a new username.", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
   
        org.getUserAccountDirectory().createUserAccount(username, password, employee, role);
        talentUA.setGraduated(true);
        
        
//        talentUA.getEmployee().setFirstName(txtFirstName.getText());
//        talentUA.getEmployee().setLastName(txtLastName.getText());
//        talentUA.getEmployee().setAddress(txtAdd.getText());
//        talentUA.getEmployee().setEmail(txtEmail.getText());
//        talentUA.setUsername(txtUsername.getText());
//        talentUA.setPassword(txtPassword.getText());
        
//        if(chkCOERole.isSelected()){
//            ua.setRoleType(Role.RoleType.CoE);
//        }
//        if(chkFinanceRole.isSelected()){
//            ua.setRoleType(Role.RoleType.Finance);
//        }
//        if(chkHRRole.isSelected()){
//            ua.setRoleType(Role.RoleType.HR);
//        }
//        if(chkManagerRole.isSelected()){
//            ua.setRoleType(Role.RoleType.Manager);
//        }
//        if(chkSupportRole.isSelected()){
//            ua.setRoleType(Role.RoleType.Support);
//        }
//        if(chkTechRole.isSelected()){
//            ua.setRoleType(Role.RoleType.Tech);
//        }
        
//        ua.setRole(ua.getRole().toString());

        JOptionPane.showMessageDialog(null, "Talent Pool candidate successfully graduated!", "Awesome!", JOptionPane.INFORMATION_MESSAGE);
        txtUsername.setText("");
        txtPassword.setText("");
    }//GEN-LAST:event_btnGradActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        userProcessContainer.remove(this);
//        Component[] componentArray = userProcessContainer.getComponents();
//        Component component = componentArray[componentArray.length - 1];
//        ViewTalentPoolJPanel viewTalentPoolJPanel = (ViewTalentPoolJPanel) component;
//        viewTalentPoolJPanel.populateTable();
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        btnGrad.setEnabled(true);
        cmbOrg.setEnabled(true);
//        chkCOEOrg.setEnabled(true);
//        chkFinanceOrg.setEnabled(true);
//        chkHROrg.setEnabled(true);
//        chkManagerOrg.setEnabled(true);
//        chkSupportOrg.setEnabled(true);
//        chkTechOrg.setEnabled(true);
//        txtFirstName.setEnabled(true);
//        txtLastName.setEnabled(true);
//        txtDOB.setEnabled(true);
//        txtAdd.setEnabled(true);
//        txtEmail.setEnabled(true);
        cmbRole.setEnabled(true);
//        chkCOERole.setEnabled(true);
//        chkFinanceRole.setEnabled(true);
//        chkHRRole.setEnabled(true);
//        chkManagerRole.setEnabled(true);
//        chkSupportRole.setEnabled(true);
//        chkTechRole.setEnabled(true);
        txtUsername.setEnabled(true);
        txtPassword.setEnabled(true);
        btnUpdate.setEnabled(false);
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void cmbOrgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbOrgActionPerformed
        org = (Organization) cmbOrg.getSelectedItem();
        if (org != null){
            popComboRole(org);
        }
    }//GEN-LAST:event_cmbOrgActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnGrad;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JComboBox cmbOrg;
    private javax.swing.JComboBox cmbRole;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField txtAdd;
    private javax.swing.JTextField txtDOB;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtFirstName;
    private javax.swing.JTextField txtLastName;
    private javax.swing.JTextField txtPassword;
    private javax.swing.JTextField txtUsername;
    // End of variables declaration//GEN-END:variables
}
