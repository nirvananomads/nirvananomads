/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Project;

import java.util.ArrayList;

/**
 *
 * @author Maxx
 */
public class ProjectDirectory {
    
    private ArrayList<Project> projectList;
    
    public ProjectDirectory(){
        projectList = new ArrayList<>();
    }

    public ArrayList<Project> getProjectList() {
        return projectList;
    }

     
    
    public Project createProject(String name){
        Project project = new Project();
        project.setName(name);
        //project.setProjectCode(projectCode);
        projectList.add(project);
        return project;
    }
}

