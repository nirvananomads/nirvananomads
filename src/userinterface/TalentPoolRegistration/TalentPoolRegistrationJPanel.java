/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.TalentPoolRegistration;

import Business.EcoSystem;
import Business.Employee.Employee;
import Business.Enterprise.Enterprise;
import Business.Enterprise.MNCEnterprise;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.Organization.TalentPoolOrganization;
import Business.Role.Role;
import Business.Role.TalentPoolRole;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Maxx
 */
public class TalentPoolRegistrationJPanel extends javax.swing.JPanel {

    /**
     * Creates new form TalentPoolRegistrationJPanel
     */
    private JPanel userProcessContainer;
    private EcoSystem system;
    private Enterprise.EnterpriseType enterpriseType;
    private Enterprise enterprise;
    
    public TalentPoolRegistrationJPanel(JPanel userProcessContainer, EcoSystem system, Enterprise.EnterpriseType enterpriseType) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.system = system;
        this.enterpriseType = enterpriseType;
        
    }
    
//    public void populateNetwork(){
//        cmbNetwork.removeAllItems();
//        for(Network network : system.getNetworkList()){
//            cmbNetwork.addItem(network);
//        }
//    }
  
    
//    public void populateOrgTypeCombo(){
//        cmbOrgType.removeAllItems();
//        cmbOrgType.addItem("");
//        for(Organization.Type type : Organization.Type.values()){
//            if(type.getValue().equals(Organization.Type.TalentPool.getValue())){
//                cmbOrgType.addItem(type);
//            }
//        }
//    }
    
//    public void popOrganizationComboBox() {
//        cmbOrgName.removeAllItems();
//        for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
//            cmbOrgName.addItem(organization);
//        }
//    }
//    
//    private void populateRoleComboBox(Organization organization){
//        cmbRole.removeAllItems();
//        cmbRole.addItem("");
//        for (Role role : organization.getSupportedRole()){
//            cmbRole.addItem(role);
//        }
//    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        firstNameTxt = new javax.swing.JTextField();
        lastNameTxt = new javax.swing.JTextField();
        dateOfBirthTxt = new javax.swing.JTextField();
        addressTxt = new javax.swing.JTextField();
        emailTxt = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        usernameTxt = new javax.swing.JTextField();
        passwordTxt = new javax.swing.JPasswordField();
        jLabel9 = new javax.swing.JLabel();
        retypePasswordTxt = new javax.swing.JPasswordField();
        registerBtn = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        jLabel13 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(45, 118, 232));
        jLabel1.setText("First Name");

        jLabel2.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(45, 118, 232));
        jLabel2.setText("Last Name");

        jLabel3.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(45, 118, 232));
        jLabel3.setText("Date of Birth");

        jLabel4.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(45, 118, 232));
        jLabel4.setText("Address");

        jLabel6.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(45, 118, 232));
        jLabel6.setText("Email");

        firstNameTxt.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        firstNameTxt.setForeground(new java.awt.Color(45, 118, 232));

        lastNameTxt.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        lastNameTxt.setForeground(new java.awt.Color(45, 118, 232));

        dateOfBirthTxt.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        dateOfBirthTxt.setForeground(new java.awt.Color(45, 118, 232));

        addressTxt.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        addressTxt.setForeground(new java.awt.Color(45, 118, 232));

        emailTxt.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        emailTxt.setForeground(new java.awt.Color(45, 118, 232));

        jLabel7.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(45, 118, 232));
        jLabel7.setText("Username");

        jLabel8.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(45, 118, 232));
        jLabel8.setText("Password");

        usernameTxt.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        usernameTxt.setForeground(new java.awt.Color(45, 118, 232));

        passwordTxt.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        passwordTxt.setForeground(new java.awt.Color(45, 118, 232));

        jLabel9.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(45, 118, 232));
        jLabel9.setText("Retype Password");

        retypePasswordTxt.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        retypePasswordTxt.setForeground(new java.awt.Color(45, 118, 232));

        registerBtn.setBackground(new java.awt.Color(255, 255, 255));
        registerBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8_Checkmark_48px.png"))); // NOI18N
        registerBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registerBtnActionPerformed(evt);
            }
        });

        btnBack.setBackground(new java.awt.Color(255, 255, 255));
        btnBack.setForeground(new java.awt.Color(45, 118, 232));
        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8_Back_50px.png"))); // NOI18N
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jLabel13.setBackground(new java.awt.Color(45, 118, 232));
        jLabel13.setFont(new java.awt.Font("Verdana", 0, 36)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/icons8_Add_User_Male_48px_1.png"))); // NOI18N
        jLabel13.setText("Talent Pool Registration");
        jLabel13.setOpaque(true);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, 876, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(registerBtn))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel6))
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(firstNameTxt)
                            .addComponent(lastNameTxt)
                            .addComponent(addressTxt)
                            .addComponent(emailTxt)
                            .addComponent(retypePasswordTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 724, Short.MAX_VALUE)
                            .addComponent(passwordTxt)
                            .addComponent(usernameTxt)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(dateOfBirthTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(65, 65, 65)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(firstNameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(lastNameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(dateOfBirthTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(addressTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(emailTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(usernameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(passwordTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(retypePasswordTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 55, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(registerBtn)
                    .addComponent(btnBack))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void registerBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registerBtnActionPerformed
        if(firstNameTxt.getText().equals("") || lastNameTxt.getText().equals("") || dateOfBirthTxt.getText().equals("") || 
                addressTxt.getText().equals("") || emailTxt.getText().equals("") || usernameTxt.getText().equals("") || 
                    passwordTxt.getText().equals("") || retypePasswordTxt.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Please complete the form.", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        
        String firstName = firstNameTxt.getText();
        String lastName = lastNameTxt.getText();
        String dateOfBirth = dateOfBirthTxt.getText();
        String address = addressTxt.getText();
        String email = emailTxt.getText();
        String username = usernameTxt.getText();
//        String role = String.valueOf(Role.RoleType.TalentPool);
        char[] passwordCharArray = passwordTxt.getPassword();
        String password = String.valueOf(passwordCharArray);
        char[] retypePasswordCharArray = retypePasswordTxt.getPassword();
        String retypePassword = String.valueOf(retypePasswordCharArray);
        
        Organization organization = new TalentPoolOrganization();
        
        Employee employee = organization.getEmployeeDirectory().createEmployee(firstName, lastName, dateOfBirth, address, email);
        
        Role role = new TalentPoolRole();
        
//        organization = new TalentPoolOrganization();
//        Employee employee = organization.getEmployeeDirectory().createEmployee(firstName, lastName, dateOfBirth, address, email);
//        Role role = new TalentPoolRole();
        
//        if(!orgComboBox.getSelectedItem().equals("Talent Pool Organization")){
//            JOptionPane.showMessageDialog(null, "Please select Talent Pool as your organization.", "Warning", JOptionPane.WARNING_MESSAGE);
//            return;
//        }
//        
//        if(!roleComboBox.getSelectedItem().equals("Talent Pool")){
//            JOptionPane.showMessageDialog(null, "Please select Talent Pool as your role.", "Warning", JOptionPane.WARNING_MESSAGE);
//            return;
//        }
//        Organization organization = new TalentPoolOrganization();
        
        
        
        if (!password.equals(retypePassword)){
            JOptionPane.showMessageDialog(null, "Passwords do not match.", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }else if(password.equals(retypePassword)){
            JOptionPane.showMessageDialog(null, "User account successfully created!", "Information", JOptionPane.INFORMATION_MESSAGE);
        }   
        
        if(system.getUserAccountDirectory().checkIfUsernameIsUnique(username) == false){
            JOptionPane.showMessageDialog(null, "Username is not available.", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }else{
            system.getUserAccountDirectory().createUserAccount(username, password, employee, role);
        }
        
//        if(system.getUserAccountDirectory().checkIfUsernameIsUnique(username) == false){
//            JOptionPane.showMessageDialog(null, "Username is not available.", "Warning", JOptionPane.WARNING_MESSAGE);
//            return;
//        }else{
//            system.getUserAccountDirectory().createUserAccount(username, password, employee, role);
//        }

//            for(Organization org : enterprise.getOrganizationDirectory().getOrganizationList()){
//                if(org instanceof TalentPoolOrganization){
//                    Employee employee = org.getEmployeeDirectory().createEmployee(firstName, lastName, dateOfBirth, address, email);
//                    Role role = new TalentPoolRole();
//                    org.getUserAccountDirectory().createUserAccount(username, password, employee, role);
//                }
//            }   
//        }
        
        firstNameTxt.setText("");
        lastNameTxt.setText("");
        dateOfBirthTxt.setText("");
        addressTxt.setText("");
        emailTxt.setText("");
        usernameTxt.setText("");
        passwordTxt.setText("");
        retypePasswordTxt.setText("");
        
//        userProcessContainer.remove(this);
//        Component[] componentArray = userProcessContainer.getComponents();
//        Component component = componentArray[componentArray.length - 1];
//        ControlJPanel manageProductCatalogJPanel = (ManageProductCatalogJPanel) component;
//        manageProductCatalogJPanel.refreshTable();
//        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
//        layout.previous(userProcessContainer);

        
    }//GEN-LAST:event_registerBtnActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        userProcessContainer.removeAll();
        JPanel blankJP = new JPanel();
        userProcessContainer.add("blank", blankJP);
        CardLayout crdLyt = (CardLayout) userProcessContainer.getLayout();
        crdLyt.next(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField addressTxt;
    private javax.swing.JButton btnBack;
    private javax.swing.JTextField dateOfBirthTxt;
    private javax.swing.JTextField emailTxt;
    private javax.swing.JTextField firstNameTxt;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField lastNameTxt;
    private javax.swing.JPasswordField passwordTxt;
    private javax.swing.JButton registerBtn;
    private javax.swing.JPasswordField retypePasswordTxt;
    private javax.swing.JTextField usernameTxt;
    // End of variables declaration//GEN-END:variables
}
