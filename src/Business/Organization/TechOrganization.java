/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.Role;
import Business.Role.TechRole;
import java.util.ArrayList;

/**
 *
 * @author Maxx
 */
public class TechOrganization extends Organization{
    
    public TechOrganization(){
        super(Organization.Type.Tech.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new TechRole());
        return roles;
    }
    
}
