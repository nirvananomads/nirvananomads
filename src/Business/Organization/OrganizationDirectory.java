/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Organization.Organization.Type;
import java.util.ArrayList;

/**
 *
 * @author Maxx
 */
public class OrganizationDirectory {
    
    private ArrayList<Organization> organizationList;
    
    public OrganizationDirectory(){
        organizationList = new ArrayList<>();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }

    public Organization createOrganization(Type type){
        Organization organization = null;
        if(type.getValue().equals(Type.CoE.getValue())){
            organization = new CoEOrganization();
            organizationList.add(organization);
        }else if(type.getValue().equals(Type.Finance.getValue())){
            organization = new FinanceOrganization();
            organizationList.add(organization);
        }else if(type.getValue().equals(Type.HR.getValue())){
            organization = new HROrganization();
            organizationList.add(organization);
        }else if(type.getValue().equals(Type.Manager.getValue())){
            organization = new ManagerOrganization();
            organizationList.add(organization);
        }else if(type.getValue().equals(Type.Support.getValue())){
            organization = new SupportOrganization();
            organizationList.add(organization);
        }else if(type.getValue().equals(Type.Tech.getValue())){
            organization = new TechOrganization();
            organizationList.add(organization);
        }else if(type.getValue().equals(Type.TalentPool.getValue())){
            organization = new TalentPoolOrganization();
            organizationList.add(organization);
        }
        return organization;
    }
    
//    public void removeOrganization(Type type){
//        organizationList.remove(type);
//    }
}
