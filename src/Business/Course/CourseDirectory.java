/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Course;

import Business.Course.Course.CourseType;
import Business.UserAccount.UserAccount;
import java.util.ArrayList;

/**
 *
 * @author Maxx
 */
public class CourseDirectory {
    
    private ArrayList<Course> courseList;
    
    public CourseDirectory(){
        courseList = new ArrayList<>();
    }

    public ArrayList<Course> getCourseList() {
        return courseList;
    }
    
    public void createCourse(Course course){
        courseList.add(course);
    }
    
    public void removeCourse(Course course){
        courseList.remove(course);
    }
    
    public Course createCourseType(CourseType courseType){
        Course course = null;
        if(courseType.getValue().equals(CourseType.Java.getValue())){
            course = new JavaCourse();
            courseList.add(course);
        }else if(courseType.getValue().equals(CourseType.Python.getValue())){
            course = new PythonCourse();
            courseList.add(course);
        }
        return course;
    }
    
//    public Course getPerformance(String courseName, int performance){
//        Course course = null;
//        if(courseName.equals(course))
//    }
}
